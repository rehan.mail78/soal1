//Created by Rehan - 081295955149  *
package main

import "fmt"

func main() {
	a, b := 3, 5
	fmt.Println("Before Swap.")
	fmt.Println("a=", a)
	fmt.Println("b=", b)
	a, b = b, a
	fmt.Println("After Swap.")
	fmt.Println("a=", a)
	fmt.Println("b=", b)

}
